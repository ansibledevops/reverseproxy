import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


def test_nginx_installed(host):
    nginx = host.package("nginx")
    print(nginx)
    assert nginx.is_installed
    # assert nginx.version.startswith("1.12.2")


def test_nginx_enabled_and_running(host):
    nginx = host.service("nginx")
    assert nginx.is_running
    assert nginx.is_enabled


def test_ssl_cert_exists(host):
    file = host.file("/etc/ssl/certs/certificate.crt")
    assert file.exists
    assert oct(file.mode) == '0644'
    assert file.user == 'root'
    assert file.group == 'root'


def test_ssl_key_folder_exists(host):
    directory = host.file("/etc/ssl/private")
    assert directory.is_directory
    assert oct(directory.mode) == '0600'


def test_ssl_key_exists(host):
    file = host.file("/etc/ssl/private/server.key")
    assert file.exists
    assert oct(file.mode) == '0600'
    assert file.user == 'root'
    assert file.group == 'root'
