Reverse Proxy with NGINX
=========

Setup NGINX as a reverse proxy server and manage configuration files for the various sites hosted.

Certificate and Private Key are stored in Ansible vault for easy deployment and portability.

Requirements
------------

None

Role Variables
--------------

```yaml
---
# defaults file for reverse_proxy
# See templates/conf.template.j2 for NGINX .conf files

ssl_cert_dir: /etc/ssl/certs/    # Location to public certificate
ssl_cert_filename: server.crt    # Filename for the certificate
ssl_key_dir: /etc/ssl/private/   # Location to private key
ssl_key_filename: server.key     # Filename for the private key

ssl_cert_data:                   # Store the certificate in Ansible vault
ssl_cert_key_data                # Store the private key in Ansible vault

domain_list:  # List of domains you want to publish through the reverse proxy.
  - domain: fully.qualified.domain.com   # External URL - If you add multiple domains, it will use the first one in the list as the file name
    ip_address: 10.1.1.1                 # Internal Server IP
    proxy_max_temp_file_size: 1024m      # See NGINX docs for definition
    proxy_read_timeout: 60s              # See NGINX docs for definition
    proxy_http_version: 1.0              # See NGINX docs for definition
    proxy_set_header: Connection close   # See NGINX docs for definition
    client_max_body_size: 1m             # See NGINX docs for definition
  - domain: domain2.domain.com           # Repeat as many times for all your services published through the proxy
    ip: 10.1.1.1
    proxy_max_temp_file_size: 1024m
    proxy_read_timeout: 60s
    proxy_http_version: 1.0
    proxy_set_header: Connection close
    client_max_body_size: 1m

    ## OPTIONAL
    rules:
      - rule: <allow/deny>
      - address: <IP or RANGE>

```

Testing
-------

Testing using Molecule has been added

1. Create a self signed certificate and private key
2. Put them under molecule/ directory
3. Update ssl_cert_data and ssl_cert_key_data to point to the self signed cert and key

```yaml
ssl_cert_data: |
  {{lookup('file', 'molecule.crt')}}
ssl_cert_key_data: |
  {{lookup('file', 'molecule.key')}}
```

Dependencies
------------

None

Example Playbook
----------------

```yaml
- hosts: servers
  become: yes
  serial: 1  # Reverse Proxy setup in HA config, only do one server at a time
  tasks:
    - import_role:
        name: reverse_proxy
```

License
-------

BSD

Todo
-------

- HA Services Installation
- HA Config

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
